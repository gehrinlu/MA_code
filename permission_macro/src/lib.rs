use proc_macro::TokenStream;
use quote::quote;
use syn::spanned::Spanned;
use syn::{
    parse_macro_input, parse_quote, Attribute, ItemFn, LitStr,
    MetaNameValue, Token,
};

fn construct_document_attribute(comment: &String) -> Attribute {
    Attribute {
        pound_token: Default::default(),
        style: syn::AttrStyle::Outer,
        bracket_token: Default::default(),
        meta: syn::Meta::NameValue(MetaNameValue {
            path: syn::parse_quote!(doc),
            eq_token: <Token![=]>::default(),
            value: parse_quote! {#comment},
        }),
    }
}

#[proc_macro_attribute]
pub fn construct_lib_function(
    attr: TokenStream,
    item: TokenStream,
) -> TokenStream {
    let args_str = parse_macro_input!(attr as LitStr);
    let permissions: Vec<_> = args_str
        .value()
        .split(',')
        .map(|s| s.trim().to_string())
        .collect();
    let mut func = parse_macro_input!(item as ItemFn);

    add_documentation(&permissions, &mut func);
    add_arguments(&permissions, &mut func);
    add_feature_condition(&permissions, &mut func);

    TokenStream::from(quote!(
            #func))
}

fn add_documentation(
    permissions: &Vec<String>,
    func: &mut ItemFn,
) {
    func.attrs.push(construct_document_attribute(
        &" # Permission".to_string(),
    ));
    for permission in permissions {
        let comment = format!(" - {:?}", &permission);
        func.attrs.push(construct_document_attribute(&comment));
    }
}

fn add_arguments(permissions: &Vec<String>, func: &mut ItemFn) {
    let mut new_inputs = quote! {};
    for permission in permissions {
        let value_name =
            format!("{}_token", permission.to_lowercase());
        let value_name_ident =
            syn::Ident::new(&value_name, func.span());
        let arg_full_path = format!("token::{}", permission);
        let perm_type =
            syn::parse_str::<syn::Path>(&arg_full_path).unwrap();
        new_inputs = quote! {
            #new_inputs
            #value_name_ident : & #perm_type,
        };
    }

    let old_inputs = &func.sig.inputs;
    if !old_inputs.is_empty() {
        func.sig.inputs = syn::parse_quote! {
            #old_inputs ,
            #new_inputs
        };
    } else {
        func.sig.inputs = syn::parse_quote! {
            #new_inputs
        };
    }
}

fn add_feature_condition(
    permissions: &Vec<String>,
    func: &mut ItemFn,
) {
    let cfg_attr: Attribute = parse_quote! (
            #[cfg(all(#(feature = #permissions),*))]
    );

    func.attrs.push(parse_quote!(#cfg_attr));
}

fn add_cfg_direct_dependency(func: &mut ItemFn) {
    let dd = "direct-dependency";
    let cfg_attr: Attribute = parse_quote! (
            #[cfg(feature = #dd )]
    );

    func.attrs.push(parse_quote!(#cfg_attr));
}

#[proc_macro_attribute]
pub fn construct_app_function(
    attr: TokenStream,
    item: TokenStream,
) -> TokenStream {
    let args_str = parse_macro_input!(attr as LitStr);
    let args_str_value = args_str.value();
    let permissions: Vec<_> = args_str_value
        .split(',')
        .map(|s| s.trim().to_string())
        .collect();
    let mut func = parse_macro_input!(item as ItemFn);

    add_call_to_lib_function(&permissions, &mut func);
    change_function_name(&mut func);
    add_cfg_direct_dependency(&mut func);
    add_feature_condition(&permissions, &mut func);

    TokenStream::from(quote!(
            #func))
}

#[proc_macro_attribute]
pub fn permissions(
    attr: TokenStream,
    item: TokenStream,
) -> TokenStream {
    let args_str = parse_macro_input!(attr as LitStr);
    let args_str_value = args_str.value();
    let permissions: Vec<_> = args_str_value
        .split(',')
        .map(|s| s.trim().to_string())
        .collect();
    let mut func = parse_macro_input!(item as ItemFn);
    let mut app_func = func.clone();

    add_documentation(&permissions, &mut func);
    add_arguments(&permissions, &mut func);
    add_feature_condition(&permissions, &mut func);

    add_call_to_lib_function(&permissions, &mut app_func);
    change_function_name(&mut app_func);
    add_cfg_direct_dependency(&mut app_func);
    add_feature_condition(&permissions, &mut app_func);

    TokenStream::from(quote!(
            #func
            #app_func))
}

fn change_function_name(func: &mut ItemFn) {
    // Add app_ in front of function name.
    func.sig.ident = syn::Ident::new(
        &format!("app_{}", func.sig.ident),
        func.sig.ident.span(),
    );
}

fn add_call_to_lib_function(
    permissions: &Vec<String>,
    func: &mut ItemFn,
) {
    let stmt = construct_local_token(&permissions);
    func.block.stmts = stmt;

    let token: syn::Expr =
        parse_quote! {localPerm::new().as_ref()};
    let tokens = vec![token; permissions.len()];

    let mut has_self = false;

    let mut argument_names = Vec::new();
    for fn_arg in &func.sig.inputs {
        let expr: syn::Expr = {
            if let syn::FnArg::Typed(pat_type) = &fn_arg {
                if let syn::Pat::Ident(pat_ident) =
                    &*pat_type.pat
                {
                    let name = &pat_ident.ident;
                    syn::Expr::Path(parse_quote!(#name))
                } else {
                    continue;
                }
            } else {
                has_self = true;
                continue;
            }
        };
        argument_names.push(expr);
    }

    let lib_func_name = &func.sig.ident;
    let stmt: syn::Stmt = {
        if has_self {
            if !argument_names.is_empty() {
                parse_quote! { return self . #lib_func_name
                ( #(#argument_names),* , #(#tokens),* ); }
            } else {
                parse_quote! { return self . #lib_func_name
                ( #(#tokens),* ); }
            }
        } else {
            if !argument_names.is_empty() {
                parse_quote! { return #lib_func_name
                ( #(#argument_names),* , #(#tokens),* ); }
            } else {
                parse_quote! { return #lib_func_name
                ( #(#tokens),* ); }
            }
        }
    };
    func.block.stmts.push(stmt);
}

fn construct_local_token(
    permissions: &Vec<String>,
) -> Vec<syn::Stmt> {
    let permissions_with_path: Vec<_> = permissions
        .iter()
        .map(|s| {
            syn::parse_str::<syn::Path>(&format!("token::{}", s))
                .unwrap()
        })
        .collect();
    parse_quote!(
         struct localPerm(());
         impl localPerm {
             const fn new() -> Self {
                localPerm(())
             }
         }
         #(
             #[cfg(feature = #permissions )]
                 impl std::convert::AsRef< #permissions_with_path > for localPerm {
                     fn as_ref(&self) -> & #permissions_with_path {
                        unsafe { std::mem::transmute(self) }
                 }
             })*
    ) //End quote
}
