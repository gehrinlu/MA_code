# Programmcode zur Masterarbeit von Lukas Gehring

- `permission_macro` enthält den Programmcode aus Kapitel 4.  
- `use_permission_macro` enthält kurze codebeispiele um an denen die macros getestet werden können.

Es empfiehlt sich das Tool [cargo watch](https://lib.rs/crates/cargo-expand) um lokal eine macroexpansion auszuführen.
