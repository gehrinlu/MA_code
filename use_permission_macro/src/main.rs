extern crate permission_macro;
use permission_macro::{construct_app_function, construct_lib_function, permissions};

mod token {
    #[derive(Debug)]
    pub struct ReadPerm(());
    pub struct WritePerm(());
}

struct LibTest {}

impl LibTest {
    #[permissions("ReadPerm, WritePerm")]
    fn fn_with_self(self, _a: (), _b: u8) {
        println!("fn_foo_with_self");
    }

    #[permissions("ReadPerm, WritePerm")]
    fn fn_with_bor_self(&self, _a: &(), _b: u8) {
        println!("fn_foo_bor_self");
    }
}

#[permissions("ReadPerm, WritePerm")]
fn fn_foo(_a: (), _b: ()) {
    println!("fn_foo");
}

#[permissions("ReadPerm, WritePerm")]
fn fn_foo_without_arg() {
    println!("fn_foo");
}

// #[permissions("ReadPerm, WritePerm")]
// fn bad() -> &token::ReadPerm {
//     return readperm_token
// }

// #[permissions("ReadPerm, WritePerm")]
// fn thread_1() {
//     let thr = thread::spawn(move || println!("Thread {:?}", readperm_token));
// }

fn main() {
    app_fn_foo((), ());
    let a = LibTest{};
    a.app_fn_with_bor_self(&(),8);
    a.app_fn_with_self((),8);
}
